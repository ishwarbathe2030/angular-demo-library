import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadImageBs64Component } from './upload-image-bs64.component';

describe('UploadImageBs64Component', () => {
  let component: UploadImageBs64Component;
  let fixture: ComponentFixture<UploadImageBs64Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadImageBs64Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadImageBs64Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
