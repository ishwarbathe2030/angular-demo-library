import { Component, OnInit } from '@angular/core';
// import {MdDialog, MdDialogRef} from '@angular/material';


// https://www.academind.com/learn/angular/snippets/angular-image-upload-made-easy/#select-a-file
@Component({
  selector: 'app-upload-image-bs64',
  templateUrl: './upload-image-bs64.component.html',
  styleUrls: ['./upload-image-bs64.component.scss']
})
export class UploadImageBs64Component implements OnInit {

  private imageSrc: string = '';
  constructor() { 

    this.imageSrc = 'ishwarbathe.jpj'

  }

  ngOnInit() {}
 

  onFileChanged(e) {
    console.log("---------------"+JSON.stringify(e)+"---------------------")
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }
  _handleReaderLoaded(e) {
    let reader = e.target;
    this.imageSrc = reader.result;
    console.log(this.imageSrc)
  }



}
