import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadImageBs64Component } from '../app/component/upload-image-bs64/upload-image-bs64.component';

const routes: Routes = [
  { path: '', 
  component: UploadImageBs64Component
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
